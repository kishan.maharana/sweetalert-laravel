<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>

    </head>
    <body>
        
        
@if(session('succmsg'))
<script>
swal("Movie Added Successfully -:)");
</script>
@endif

<center><h1>SWEET ALERT IN LARAVEL</h1></center>

<table>
    <tr>
        <th>id</th>
        <th>Name</th>
    </tr>

    @foreach($sweet as $item)
        
        <tr>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
        </tr>

    @endforeach


</table>

    </body>
</html>
